import healthApi from "../routes/health/health.routes";
import registrationApi from "../routes/registration/registration.routes";
import blogsApi from "../routes/blogs/blogs.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/registration', endpointLogger, registrationApi);
    app.use('/blogs', endpointLogger, blogsApi);
}

export default routes;

