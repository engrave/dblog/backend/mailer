const nodemailer = require('nodemailer');
const mg = require('nodemailer-mailgun-transport');
const secrets = require('@cloudreach/docker-secrets')
const assert = require('assert');

assert(secrets.MAILGUN_API_KEY, "MAILGUN_API_KEY docker secret is required");
assert(secrets.MAILGUN_DOMAIN, "MAILGUN_DOMAIN docker secret is required");
assert(process.env.MAILGUN_SENDER_NAME, "MAILGUN_SENDER_NAME environment variable is required");
assert(process.env.MAILGUN_SENDER_ADDRESS, "MAILGUN_SENDER_ADDRESS environment variable is required");

const auth = {
    auth: {
        api_key: secrets.MAILGUN_API_KEY,
        domain: secrets.MAILGUN_DOMAIN
    }
}

const engine = nodemailer.createTransport(mg(auth));

const sender_name = process.env.MAILGUN_SENDER_NAME;
const sender_address = process.env.MAILGUN_SENDER_ADDRESS;


export {
    engine,
    sender_address,
    sender_name
};