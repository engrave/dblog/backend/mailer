import { Request, Response } from 'express';
import { handleResponseError, generateHealthResponse } from '../../../submodules/shared-library';

const middleware: any[] =  [
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {
        return res.json(generateHealthResponse());
    }, req, res);
}

export default {
    middleware,
    handler
}